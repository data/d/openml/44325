# OpenML dataset: Meta_Album_ACT_40_Extended

https://www.openml.org/d/44325

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Stanford 40 Actions Dataset (Extended)**
***
The Stanford 40 Actions dataset(http://vision.stanford.edu/Datasets/40actions.html) contains images of humans performing 40 actions. There are 9 532 images in total with 180-300 images per action class. The dataset is designed for understanding human actions in still images. For Meta-Album, the dataset is preprocessed and images are resized into 128x128 pixels using Open-CV.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/ACT_40.png)

**Meta Album ID**: HUM_ACT.ACT_40  
**Meta Album URL**: [https://meta-album.github.io/datasets/ACT_40.html](https://meta-album.github.io/datasets/ACT_40.html)  
**Domain ID**: HUM_ACT  
**Domain Name**: Human Actions  
**Dataset ID**: ACT_40  
**Dataset Name**: Stanford 40 Actions  
**Short Description**: Stanford 40 Actions dataset with images of humans performing 40 actions.  
**\# Classes**: 40  
**\# Images**: 3389  
**Keywords**: human actions  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Cite paper to use dataset  
**License (Meta-Album data release)**: CC BY-NC 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  

**Source**: Stanford 40 Actions  
**Source URL**: http://vision.stanford.edu/Datasets/40actions.html  
  
**Original Author**: B. Yao, X. Jiang, A. Khosla, A.L. Lin, L.J. Guibas, and L. Fei-Fei.  
**Original contact**: bangpeng@cs.stanford.edu  

**Meta Album author**: Jilin He  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@INPROCEEDINGS{6126386,
  author={Yao, Bangpeng and Jiang, Xiaoye and Khosla, Aditya and Lin, Andy Lai and Guibas, Leonidas and Fei-Fei, Li},
  booktitle={2011 International Conference on Computer Vision}, 
  title={Human action recognition by learning bases of action attributes and parts}, 
  year={2011},
  pages={1331-1338},
  doi={10.1109/ICCV.2011.6126386}
}

```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44247)  [[Mini]](https://www.openml.org/d/44291)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44325) of an [OpenML dataset](https://www.openml.org/d/44325). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44325/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44325/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44325/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

